class CreatePerceptrons < ActiveRecord::Migration[6.0]
  def change
    create_table :perceptrons do |t|
      t.text :feature_set
      t.text :feature_labels
      t.float :learning_rate
      t.integer :epochs
      t.text :weights
      t.float :bias
      t.float :error
      t.text :weighted_sum

      t.timestamps
    end
  end
end
